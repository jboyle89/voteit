# VoteIt README #

VoteIt is a Reddit/Digg-type website designed in ASP.NET MVC 5.

### How do I get set up? ###

* Simply download the source, and load the solution in VS2015 or later.
* Remember to change the connection string in web.config in the WebUI project to match your database settings. VoteIt uses Entity Framework's Code First feature, so a connection to a valid data source is all that is required.

### Who do I talk to? ###

* Admin: jboyle1989@gmail.com