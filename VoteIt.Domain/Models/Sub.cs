﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoteIt.Domain.Models
{
    public class Sub
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string SideBarText { get; set; }

        public DateTime Created { get; set; }
    }
}
