﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoteIt.Domain.Models
{
    public class Submission
    {
        public int Id { get; set; }

        public ApplicationUser UserSubmitted { get; set; }

        public Sub Sub { get; set; }

        public bool IsTextPost { get; set; }

        public String Title { get; set; }

        public String Text { get; set; }

        public String Link { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }
    }
}
