﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoteIt.Domain.Models
{
    public class Comment
    {
        public int Id { get; set; }

        public ApplicationUser User { get; set; }

        public String CommentText { get; set; }

        public Comment Parent { get; set; }

        public IEnumerable<Comment> Children { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }
    }
}
